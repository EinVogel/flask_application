import psycopg2
import yaml
from prettytable import PrettyTable


def query_from_database(date_from, date_to):
    # Open and read yaml
    file_descriptor = open("configuration.yaml", 'r')
    # load the yaml file to the variable of data
    data = yaml.load(file_descriptor, Loader=yaml.FullLoader)

    # Connect to the database
    conn = psycopg2.connect(database=data["db_name"], user=data["db_user"], password=data["db_pass"],
                            host=data["db_host"], port=data["db_port"])

    cur = conn.cursor()
    # Do some request SQL
    cur.execute("SELECT id, country, manufacturer, status, updated_at FROM drugstore_positions "
                "WHERE updated_at BETWEEN '{}' AND '{}' LIMIT 10".format(date_from, date_to))
    # Take all rows
    row = cur.fetchall()

    # Use the table library
    table = PrettyTable()
    table.field_names = ['id', 'country', 'manufacturer', 'status', 'updated_at']

    # Fill the table
    label = 0
    for i in row:
        # add element to the table
        table.add_row(row[label])
        label += 1

    return str(table)
