import psycopg2

db_name = 'xyrshsim'
db_user = 'xyrshsim'
db_pass = 'boYIsPfD0nTwAyBskUBytlcuJ-uE61ic'
db_host = 'salt.db.elephantsql.com'
db_port = '5432'

conn = psycopg2.connect(database=db_name, user=db_user, password=db_pass, host=db_host, port=db_port)
print('Database connected successfully')

# Create table Employer

'''''
cur = conn.cursor()
cur.execute("""

CREATE TABLE Employer
(
ID INT PRIMARY KEY NOT NULL,
NAME TEXT NOT NULL,
EMAIL TEXT NOT NULL
)

""")
print("Table created successfully")
'''''

# Add data to the Employer

'''''
cur = conn.cursor()
cur.execute("INSERT INTO Employer (ID, NAME, EMAIL) VALUES (2, 'John', 'john@gmail.com')")

conn.commit()
print('Data inserted successfully')
conn.close()
'''''

# Select from data

'''''
cur = conn.cursor()
cur.execute("SELECT ID, NAME, EMAIL FROM Employer")
row = cur.fetchall()
print(row[0])
print('Data selected successfully')
conn.close()
'''''

# Update data

'''''
cur = conn.cursor()
cur.execute("UPDATE Employer set EMAIL = 'udate@gmail.com' WHERE ID = 1")
cur.execute("SELECT * FROM Employer ORDER BY ID ASC")
row = cur.fetchall()
print(row)
conn.commit()
print('Data updated successfully')
conn.close()
'''''

# Delete data

'''''
cur = conn.cursor()
cur.execute("DELETE FROM Employer WHERE ID = 1")
conn.commit()
print('Data updated successfully')
'''''
