from Euler_tasks.Euler_task_1 import euler_1
from Euler_tasks.Euler_task_2 import euler_2
from Euler_tasks.Euler_task_3 import euler_3
from Euler_tasks.Euler_task_4 import euler_4
from Euler_tasks.Euler_task_5 import euler_5
from Euler_tasks.Euler_task_6 import euler_6
from Euler_tasks.Euler_task_7 import euler_7
from Euler_tasks.Euler_task_8 import euler_8
from Euler_tasks.Euler_task_9 import euler_9
from Euler_tasks.Euler_task_10 import euler_10
from Euler_tasks.Euler_task_11 import euler_11
from Euler_tasks.Euler_task_12 import euler_12
from database import query_from_database


from flask import Flask, request
app = Flask(__name__)


@app.route('/<int:number>/')
def query(number):
    if number == 1:
        return euler_1()
    elif number == 2:
        return euler_2()
    elif number == 3:
        return euler_3()
    elif number == 4:
        return euler_4()
    elif number == 5:
        return euler_5()
    elif number == 6:
        return euler_6()
    elif number == 7:
        return euler_7()
    elif number == 8:
        return euler_8()
    elif number == 9:
        return euler_9()
    elif number == 10:
        return euler_10()
    elif number == 11:
        return euler_11()
    elif number == 12:
        return euler_12()
    else:
        return 'The tasks after 12 has not completed yet'


@app.route('/data')
def query_data():
    date_from = request.form.get('date_from')
    date_to = request.form.get('date_to')
    return query_from_database(date_from, date_to)


if __name__ == '__main__':
    app.run(debug=True)



