import time


def euler_1():
    """""
    Euler task 1
    If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
    The sum of these multiples is 23.
    Find the sum of all the multiples of 3 or 5 below 1000.
    """""

    # Start of time
    start = time.time()

    # Create an array with multiples of 3 and 5
    a = [x for x in range(1, 1000) if x % 3 == 0 or x % 5 == 0]

    # End of time
    end = time.time()
    # We can see the program execution time
    answer_1 = 'The sum of all the multiples of 3 or 5 below 1000 is' + ' ' + str(sum(a))
    answer_2 = 'The program execution time is' + ' ' + str(end - start)
    return 'Euler task 1' + '\n' + answer_1 + '\n' + answer_2
