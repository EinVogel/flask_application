import time


def euler_4():
    """""
    Euler task 4
    A palindromic number reads the same both ways.
     The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
    Find the largest palindrome made from the product of two 3-digit numbers.
    """""

    # Start of time
    start = time.time()

    max_symbols = 999
    min_symbols = 100
    label = 0
    for i in range(max_symbols, min_symbols, -1):
        for element in range(max_symbols, min_symbols, -1):
            palindrome = i * element
            palindrome = str(palindrome)
            if palindrome[0] == palindrome[-1] and palindrome[1] == palindrome[-2] and palindrome[2] == palindrome[-3]:
                label = 1
                break
        if label == 1:
            break

    # End of time
    end = time.time()
    # We can see the program execution time
    answer_1 = 'The largest palindrome made from the product of two 3-digit numbers is' + ' ' + str(palindrome)
    answer_2 = 'The program execution time is' + ' ' + str(end - start)
    return 'Euler task 4' + '\n' + answer_1 + '\n' + answer_2
