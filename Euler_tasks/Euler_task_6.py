import time


def euler_6():
    """""
    Euler task 6
    The sum of the squares of the first ten natural numbers is,

    12 + 22 + ... + 102 = 385
    The square of the sum of the first ten natural numbers is,

    (1 + 2 + ... + 10)2 = 552 = 3025
    Hence the difference between the sum of the squares of the first ten natural
    numbers and the square of the sum is 3025 − 385 = 2640.

    Find the difference between the sum of the squares of the first
    one hundred natural numbers and the square of the sum.
    """""

    # Start of time
    start = time.time()

    end_of_number = 100
    stop = end_of_number + 1
    a = 0
    b = 0
    for i in range(1, stop):
        a = a + i ** 2
        b = b + i
    b = b ** 2

    # End of time
    end = time.time()
    # We can see the program execution time
    answer_1 = 'The difference between the sum of the squares of ' \
               'the first one hundred natural numbers and the square of the sum is' + ' ' + str(b - a)
    answer_2 = 'The program execution time is' + ' ' + str(end - start)
    return 'Euler task 6' + '\n' + answer_1 + '\n' + answer_2
