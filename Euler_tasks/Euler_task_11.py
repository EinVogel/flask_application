import numpy as np
import time


def euler_11():
    """""
    Euler task 11
    In the 20×20 grid below, four numbers along a diagonal line have been marked in red.
    The product of these numbers is 26 × 63 × 78 × 14 = 1788696.

    What is the greatest product of four adjacent numbers in the same direction (up, down, left, right, or diagonally)
     in the 20×20 grid?
    """""

    # Start of time
    start = time.time()

    a = open("Euler_task_11.txt")
    b = (a.read())
    b = b.replace("\n", ' ')
    arr = []
    arr_of_numbers = []
    for i in b:
        arr.append(i)
    m = 0
    k = 1
    for i in range(0, len(arr), 3):
        number = arr[m] + arr[k]
        number = int(number)
        arr_of_numbers.append(number)
        m += 3
        k += 3

    arr_of_numbers = np.array(arr_of_numbers)
    # converting a line array to 20 x 20
    arr_of_numbers.shape = (20, 20)

    f = 1
    list_of_prod_1 = []
    list_of_prod_2 = []
    list_of_prod_3 = []
    list_of_prod_4 = []
    for i in range(0, 17):  # 17 because the main matrix 20-4 small matrix == 17. Loop vertically.
        for element in range(0, 17):
            temp_matrix = arr_of_numbers[i:i + 4, element:element + 4]  # tempMatrix temporary array 4 х 4
            #  Loop to find the product of diagonal 1
            for cell in range(0, 4):
                f = f * temp_matrix[cell, cell]
            list_of_prod_1.append(f)
            f = 1
            #  Loop to find the product of diagonal 2
            for cell in range(0, 4):
                f = f * temp_matrix[3 - cell, cell]
            list_of_prod_2.append(f)
            f = 1
            #  Loop to find the product along a horizontal line
            for cell in range(0, 4):
                for s in range(0, 4):
                    f = f * temp_matrix[cell, s]
                list_of_prod_3.append(f)
                f = 1
            #  Loop to find the product along a vertical line
            for cell in range(0, 4):
                for s in range(0, 4):
                    f = f * temp_matrix[s, cell]
                list_of_prod_4.append(f)
                f = 1
    arr_of_answers = [max(list_of_prod_1), max(list_of_prod_2), max(list_of_prod_3), max(list_of_prod_4)]

    # End of time
    end = time.time()
    # We can see the program execution time
    answer_1 = 'The greatest product of four adjacent numbers in the same direction is' + ' ' + str(max(arr_of_answers))
    answer_2 = 'The program execution time is' + ' ' + str(end - start)
    return 'Euler task 11' + '\n' + answer_1 + '\n' + answer_2
