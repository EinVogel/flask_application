import time


def euler_8():
    """""
    Euler task 8
    The four adjacent digits in the 1000-digit number that have the greatest product are 9 × 9 × 8 × 9 = 5832.
    Find the thirteen adjacent digits in the 1000-digit number that have the greatest product.
    What is the value of this product?
    """""

    # Start of time
    start = time.time()
    # open the file
    a = open("Euler_task_8.txt")
    # read
    b = (a.read())
    # remove the line break character
    b = b.replace("\n", '')
    arr = []
    temp_arr = []
    max_prod = []
    # The product of how many numbers?
    numbers = 13
    for i in b:
        arr.append(i)
    m = 0
    for i in range(1, int(1000 - numbers + 2)):
        for element in range(0, numbers):
            temp_arr.append(int(arr[m + element]))
        prod_of_numbers = 1
        for k in temp_arr:
            prod_of_numbers = prod_of_numbers * k
        max_prod.append(prod_of_numbers)
        temp_arr = []
        m += 1

    # End of time
    end = time.time()
    # We can see the program execution time
    answer_1 = 'The thirteen adjacent digits in the 1000-digit' \
               ' number that have the greatest product is' + ' ' + str(max(max_prod))
    answer_2 = 'The program execution time is' + ' ' + str(end - start)
    return 'Euler task 8' + '\n' + answer_1 + '\n' + answer_2

