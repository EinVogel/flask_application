import time


def euler_5():
    """""
    Euler task 5
    2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

    What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
    """""

    # Start of time
    start = time.time()

    end_of_numbers = 20
    prod_of_numbers = 1
    stop = end_of_numbers + 1
    arr = []
    list_of_prime = []
    multiplier = 1
    # Find a list of Prime numbers in the range from 1 to endOfNumbers
    for i in range(1, stop):
        for element in range(1, stop):
            m = i % element
            arr.append(m)
        if arr.count(0) == 2:
            list_of_prime.append(i)
        arr = []

    for i in list_of_prime:
        prod_of_numbers = prod_of_numbers * i

    label = 0
    for i in range(1, 100000000000000000):
        for element in range(1, stop):
            if prod_of_numbers * i % element == 0:
                label = 1
                continue
            else:
                label = 0
                break
        if label == 1:
            multiplier = i
            break
        else:
            i += 1

    divider = prod_of_numbers * multiplier

    # End of time
    end = time.time()
    # We can see the program execution time
    answer_1 = 'The smallest positive number that is evenly divisible by all of the numbers from 1 to 20 is'\
               + ' ' + str(divider)
    answer_2 = 'The program execution time is' + ' ' + str(end - start)
    return 'Euler task 5' + '\n' + answer_1 + '\n' + answer_2
